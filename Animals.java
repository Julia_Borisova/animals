package bus;

/**
 * Абстрактный класс Animals.
 */
public abstract class Animals {

    public static final String OUTPUT_FORMAT_LINE = "%s говорит '%s'.";
    private String name;
    private String voice;

    protected Animals(String name, String voice) {
        this.name = name;
        this.voice = voice;
    }

    /**
     * Метод возвращает название животного.
     * @return название животного.
     */
    public String getName() {
        return name;
    }

    /**
     * Метод возвращает голос/слова животного.
     * @return голос/слова животного.
     */
    public String getVoice() {
        return voice;
    }

    /**
     * Выводит сообщение по заданному формату.
     */
    public void printDisplay(){
        System.out.println(String.format(
                OUTPUT_FORMAT_LINE,
                name,
                voice
        ));
    }
}
