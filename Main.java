package bus;

public class Main {
    public static void main(String... args) {
        Dog dog = new Dog();
        Dog dog1 = new Dog("Собака","гав-гав");
        Dog dog2 =new Dog("Бобик","тяф-тяф");

        dog.printDisplay();
        dog1.printDisplay();
        dog2.printDisplay();

        Cat cat = new Cat();
        Cat cat1 = new Cat("Кошка","мяу-мяу");
        Cat cat2 = new Cat("Барсик","Мурр!");

        cat.printDisplay();
        cat1.printDisplay();
        cat2.printDisplay();

        Unicorn unicorn= new Unicorn();
        Unicorn unicorn1 = new Unicorn("Единорог","иго-го");
        Unicorn unicorn2 = new Unicorn("Верджиния","Фрр!");

       unicorn.printDisplay();
       unicorn1.printDisplay();
       unicorn2.printDisplay();

    }

}
